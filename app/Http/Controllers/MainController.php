<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SSO\SSO;

class MainController extends Controller
{
    public function index()
    {
    	echo "hello world";
    }

    public function logout()
    {
    	return SSO::logout();
    }
}
